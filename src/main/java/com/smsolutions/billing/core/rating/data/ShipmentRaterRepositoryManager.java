package com.smsolutions.billing.core.rating.data;

import com.smsolutions.billing.repository.impl.specific.CountryJpaRepository;
import com.smsolutions.billing.repository.impl.specific.CountryRangesJpaRepository;
import com.smsolutions.billing.repository.impl.specific.CurrencyJpaRepository;
import com.smsolutions.billing.repository.impl.specific.WeightRangesJpaRepository;
import com.smsolutions.billing.repository.impl.specific.WeightToCountryCostJpaRepository;
import com.smsolutions.billing.repository.impl.specific.WeightToCountryRangesJpaRepository;

public class ShipmentRaterRepositoryManager extends RepositoryManager {
	
	private final String persistenceUnitName="hibernateCustomUnit";
	private CountryJpaRepository countryRepository;
	private CountryRangesJpaRepository countryRangesRepository;
	private WeightRangesJpaRepository weightRangesRepository;
	private WeightToCountryRangesJpaRepository weightToCountryRangesRepository;
	private WeightToCountryCostJpaRepository weightToCountryCostRepository;
	private CurrencyJpaRepository currencyRepository;
	
	public ShipmentRaterRepositoryManager(){
		countryRepository=new CountryJpaRepository(persistenceUnitName);
		countryRangesRepository=new CountryRangesJpaRepository(persistenceUnitName);
		weightRangesRepository=new WeightRangesJpaRepository(persistenceUnitName);
		weightToCountryRangesRepository=new WeightToCountryRangesJpaRepository(persistenceUnitName);
		weightToCountryCostRepository=new WeightToCountryCostJpaRepository(persistenceUnitName);
		currencyRepository=new CurrencyJpaRepository(persistenceUnitName);
	}

	private int getCountryIdByName(String countryName){
		return countryRepository.findByName(countryName);
	}
	
	private int getCurrencyIdByName(String currencyName){
		return currencyRepository.findByName(currencyName);
	}
	
	public int mapCountryRanges(String countryOfOrigin,String countryOfDestination){
		int countryOfOriginId=getCountryIdByName(countryOfOrigin);
		int countryOfDestinationId=getCountryIdByName(countryOfDestination);
		return countryRangesRepository.findRangeByIds(countryOfOriginId, countryOfDestinationId);
	}
	
    public int mapWeightRanges(double weight){
    	return weightRangesRepository.getRangeIdByWeight(weight);
	}
    
    public int mapWeightToCountryRanges(int weight,int country){
    	return weightToCountryRangesRepository.getRangeIdByWeightAndCountry(weight,country);
	}
    
    public double mapWeightToCountryCost(int wcr,String currencyName){
    	int currencyId=getCurrencyIdByName(currencyName);
    	return weightToCountryCostRepository.getRangeIdByWCRAndCurrency(wcr,currencyId);
	}
}
