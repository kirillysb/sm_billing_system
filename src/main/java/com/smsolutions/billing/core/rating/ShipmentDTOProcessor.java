package com.smsolutions.billing.core.rating;

import java.util.Iterator;

import com.smsolutions.billing.core.rating.calculator.TwacMetricsCalculator;
import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.Shipment;

public class ShipmentDTOProcessor extends DTOProcessor {
	
	private ShipmentRater rater;
	private TwacMetricsCalculator calculator;
	private final int expressRate=2;
	
	public ShipmentDTOProcessor(){
		rater=new ShipmentRater();
		calculator= new TwacMetricsCalculator();
	}
	
	public TotalWeightAndCostResponse proc(OrderDetailedRecord<Shipment> odr){
		for(Shipment shipment : odr) {
		    double rate = rater.rate(shipment,odr.getCurrency());
		    calculator.addWeight(shipment.getItemWeight());
		    if(shipment.getIsExpress())
		    calculator.addCost(rate*expressRate);
		    else calculator.addCost(rate);
		}
		return calculator.getResponse();
	}

}
