package com.smsolutions.billing.core.rating;

import java.util.Queue;

import com.smsolutions.billing.core.rating.calculator.TwacMetricsCalculator;
import com.smsolutions.billing.repository.impl.specific.entities.Shipment;

public class Worker implements Runnable {

	private Thread t;
	private Queue<Shipment> queue;
	private TwacMetricsCalculator calc;
	private Queue<Shipment> resultQueue;

	Worker(Queue<Shipment> queue,Queue<Shipment> resultQueue) {
		this.queue=queue;
		this.resultQueue=resultQueue;
		calc=new TwacMetricsCalculator();
	}

	public void run() {
		//System.out.println("Running " +  threadName );
		try {
			while(true){
				if(queue.isEmpty())
					break;
				
				Shipment tempShipment=queue.poll();
				//double tempCost=(double)calc.calculateCost(tempShipment);
				//tempShipment.totalCost=tempCost;
				resultQueue.add(tempShipment);
			}
		}catch (Exception e) {
			//System.out.println("Thread " +  threadName + " interrupted.");
		}
		//System.out.println("Thread " +  threadName + " exiting.");
	}

	public void start () {
		//System.out.println("Starting " +  threadName );
		if (t == null) {
			t = new Thread(this);
			t.start();
		}
	}

}
