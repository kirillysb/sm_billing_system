package com.smsolutions.billing.core.rating.exceptions;

public class QueueIsNotLoadedException extends Exception {

	public QueueIsNotLoadedException(String message){
		super(message);
	}

}
