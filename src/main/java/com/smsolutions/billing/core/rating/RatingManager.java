package com.smsolutions.billing.core.rating;

import java.util.List;

import com.smsolutions.billing.repository.impl.specific.entities.Shipment;

public class RatingManager {

	private RatingProcessor processor;
	
	public RatingManager(int countOfThreads){
		processor=new RatingProcessor(countOfThreads);
		
	}
	
	public RatingManager(){
		processor=new RatingProcessor();
	}
	
	public void loadElements(List<Shipment> data){
		processor.loadQueue(data);
	}
	
	
	
	public boolean isReady(){
		return processor.ready();
	}
	
	public void start() throws Exception{
		processor.start();
	}
	
	

}
