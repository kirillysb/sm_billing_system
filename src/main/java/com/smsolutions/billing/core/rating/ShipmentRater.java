package com.smsolutions.billing.core.rating;

import com.smsolutions.billing.core.rating.data.ShipmentRaterRepositoryManager;
import com.smsolutions.billing.core.rating.exceptions.InvalidAttributeOfRateableObjectException;
import com.smsolutions.billing.mediation.dto.Shipment;

public class ShipmentRater extends Rater<Shipment>{
	
	private ShipmentRaterRepositoryManager rpmanager;
	
	public ShipmentRater(){
		rpmanager=new ShipmentRaterRepositoryManager();
	}
	
	@Override
	public double rate(Shipment shipment,String currency){
		int countryRange=mapCountryRanges(shipment.getCountryOfOrigin().toUpperCase(),shipment.getCountryOfDestination().toUpperCase());
		int weightRange=mapWeightRanges(shipment.getItemWeight());
		int wcr = mapWeightToCountryRanges(weightRange,countryRange);
		double rate=mapWeightToCountryCost(wcr,currency.toUpperCase());
		
		return rate;
	}
	
	private int mapCountryRanges(String countryOfOrigin,String countryOfDestination){
		return rpmanager.mapCountryRanges(countryOfOrigin,countryOfDestination);
	}
	
    private int mapWeightRanges(double weight){
    	return rpmanager.mapWeightRanges(weight);
	}
    
    private int mapWeightToCountryRanges(int weight,int country){
    	return rpmanager.mapWeightToCountryRanges(weight,country);
	}
    
    private double mapWeightToCountryCost(int wcr,String currency){
    	return rpmanager.mapWeightToCountryCost(wcr,currency);
	}
    

}
