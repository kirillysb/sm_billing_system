package com.smsolutions.billing.core.rating.exceptions;

public class InvalidAttributeOfRateableObjectException extends Exception {

	public InvalidAttributeOfRateableObjectException(String message){
		super(message);
	}

}
