package com.smsolutions.billing.core.rating.calculator;

import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;
import com.smsolutions.billing.repository.impl.specific.entities.Shipment;

public class TwacMetricsCalculator extends MetricsCalculator<TotalWeightAndCostResponse>{
	
	private double totalCost=0;
	private double totalWeight=0;
	
	public TwacMetricsCalculator(){
		
	}
	
	public void addWeight(double weight){
		this.totalWeight+=weight;
	}
	
	public void addCost(double cost){
		this.totalCost+=cost;
	}
	
	@Override
	public TotalWeightAndCostResponse getResponse(){
		return new TotalWeightAndCostResponse(totalWeight,totalCost);
	}
}
