package com.smsolutions.billing.core.rating;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.smsolutions.billing.core.rating.Worker;
import com.smsolutions.billing.core.rating.exceptions.QueueIsNotLoadedException;
import com.smsolutions.billing.repository.impl.specific.entities.Shipment;

public class RatingProcessor {
	
	private int countOfThreads=2;
	private Queue<Shipment> queue;
	private Queue<Shipment> resultQueue;

	RatingProcessor(int countOfThreads){
		this.countOfThreads=countOfThreads;
		queue=new ConcurrentLinkedQueue<Shipment>();
		resultQueue=new ConcurrentLinkedQueue<Shipment>();
	}

	RatingProcessor(){
		queue=new ConcurrentLinkedQueue<Shipment>();
	}
	
	public boolean ready(){
		if(queue.isEmpty()) return false;
		else return true;
	}
	
	public void loadQueue(Collection<Shipment> collection){
		queue.addAll(collection);
	}
	
	public Queue<Shipment> getResultQueue(){
		return this.resultQueue;
	}
	
	public void start() throws Exception{
		
		if(ready()) {
		List<Worker> workers=new ArrayList<Worker>();
		
		for(int i=0;i<countOfThreads;i++){
			workers.add(new Worker(queue,resultQueue));
		}
		
		for(Iterator<Worker> i = workers.iterator(); i.hasNext(); ) {
		    Worker item = i.next();
		    item.start();
		    //System.out.println(item);
		}
		}
		else throw new QueueIsNotLoadedException("Processor is not ready!");
	}

}
