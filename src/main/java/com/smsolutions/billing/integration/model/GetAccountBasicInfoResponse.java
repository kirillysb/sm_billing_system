package com.smsolutions.billing.integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetAccountBasicInfoResponse {
	
	@XmlElement(required = true)
    private String userAge;
	
	public String getUserAge() {
		return userAge;
	}

	public void setExtractedCost(String userAge) {
		this.userAge = userAge;
	}


}
