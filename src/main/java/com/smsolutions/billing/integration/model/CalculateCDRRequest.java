package com.smsolutions.billing.integration.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.smsolutions.billing.mediation.dto.Shipment;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CalculateCDRRequest {
	 
	 @XmlElement(name="shipments",required = true)
	    private List<Shipment> shipments;
	 
	 @XmlElement(name="currency",required = true)
	    private String currency;
	    
	    public List<Shipment> getShipments(){
	    	return shipments;
	    }
	    
	    public void setShipments(List<Shipment> shipments){
	    	this.shipments=shipments;
	    }
	    
	    public String getCurrency(){
	    	return currency;
	    }
	    
	    public void setCurrency(String currency){
	    	this.currency=currency;
	    }

}
