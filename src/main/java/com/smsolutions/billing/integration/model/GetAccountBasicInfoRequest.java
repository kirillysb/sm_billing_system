package com.smsolutions.billing.integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetAccountBasicInfoRequest {
	
	 @XmlElement(required = true)
	    private int uid;

	    public int getUID() {
	        return uid;
	    }

	    public void setUID(int uid) {
	        this.uid = uid;
	    }
	    
}