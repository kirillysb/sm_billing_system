package com.smsolutions.billing.integration.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.smsolutions.billing.repository.impl.specific.entities.Order;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetUserOrderHistoryResponse {

	@XmlElement(name="order",required = true)
    private List<Order> orders;

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
}
