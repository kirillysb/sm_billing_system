package com.smsolutions.billing.integration.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetUserOrderHistoryRequest {

	@XmlElement(name="username",required = true)
	private String usernamae;

	@XmlElement(name="indexStart",required = true)
	private int indexStart;

	@XmlElement(name="indexStop",required = true)
	private int indexStop;

	public String getUsernamae() {
		return usernamae;
	}

	public void setUsernamae(String usernamae) {
		this.usernamae = usernamae;
	}

	public int getIndexStart() {
		return indexStart;
	}

	public void setIndexStart(int indexStart) {
		this.indexStart = indexStart;
	}

	public int getIndexStop() {
		return indexStop;
	}

	public void setIndexStop(int indexStop) {
		this.indexStop = indexStop;
	}

}
