package com.smsolutions.billing.integration.exceptions;

public class DTObjectEmptyBodyException extends Exception {

	public DTObjectEmptyBodyException(String message){
		super(message);
	}
}
