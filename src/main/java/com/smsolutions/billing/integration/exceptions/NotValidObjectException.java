package com.smsolutions.billing.integration.exceptions;

public class NotValidObjectException extends Exception {

	public NotValidObjectException(String message){
		super(message);
	}
}
