package com.smsolutions.billing.integration.generation;

import com.smsolutions.billing.integration.exceptions.DTObjectEmptyBodyException;
import com.smsolutions.billing.integration.exceptions.NotValidObjectException;
import com.smsolutions.billing.mediation.ShipmentDTOForwarder;
import com.smsolutions.billing.repository.impl.specific.OrdersJpaRepository;

public class OrderHistoryResponseGenerator implements IResponseGenerator<OrderHistoryResponse>{
	
	private String username;
	private int indexStart;
	private int indexStop; 
	
	private OrdersJpaRepository repo;
	
	public OrderHistoryResponseGenerator(String username,int indexStart,int indexStop){
		this.username=username;
		this.indexStart=indexStart;
		this.indexStop=indexStop;
		repo = new OrdersJpaRepository("hibernateCustomUnit");
	}

	@Override
	public OrderHistoryResponse produceResponse() throws DTObjectEmptyBodyException, NotValidObjectException {
		if(hasSomethingToProduceFrom()){
			Response response = repo.getResponseByUsername(username, indexStart, indexStop-indexStart);
			if(!(response instanceof NotValidObjectResponse))
				return (OrderHistoryResponse)response;
			else throw new NotValidObjectException("Passed object is not a valid object in the current context!");
		}
		else throw new DTObjectEmptyBodyException("Data Transfer Object's body can not be empty!");
	}

	@Override
	public boolean hasSomethingToProduceFrom() {
		//boolean usernameNotEmptyOrNull=;
		//boolean indexStartAndStopNotNegative = ;
		return (username!=""&&username!=null)&&(indexStart>=0 && indexStop>=0);
	}
	

}
