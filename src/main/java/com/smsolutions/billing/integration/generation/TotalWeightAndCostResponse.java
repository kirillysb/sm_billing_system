package com.smsolutions.billing.integration.generation;

public class TotalWeightAndCostResponse extends Response {
	
	private double totalWeight;
	private double totalCost;
	
	public TotalWeightAndCostResponse(){
		
	}
	
    public TotalWeightAndCostResponse(double totalWeight,double totalCost){
		this.totalWeight=totalWeight;
		this.totalCost=totalCost;
	}
	
	public double getTotalWeight(){
		return this.totalWeight;
	}
	
	public void setTotalWeight(double totalWeight){
		this.totalWeight=totalWeight;
	}
	
	public double getTotalCost(){
		return this.totalCost;
	}
	
	public void setTotalCost(double totalCost){
		this.totalCost=totalCost;
	}

}
