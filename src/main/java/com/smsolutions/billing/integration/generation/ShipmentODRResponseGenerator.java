package com.smsolutions.billing.integration.generation;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.smsolutions.billing.integration.exceptions.DTObjectEmptyBodyException;
import com.smsolutions.billing.integration.exceptions.NotValidObjectException;
import com.smsolutions.billing.mediation.DataTransferObjectForwarder;
import com.smsolutions.billing.mediation.ShipmentDTOForwarder;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.Shipment;
import com.smsolutions.billing.integration.generation.Response;
import com.smsolutions.billing.integration.generation.NotValidObjectResponse;
import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;


@Configurable
public class ShipmentODRResponseGenerator implements IResponseGenerator<TotalWeightAndCostResponse>,
                                                          IReusable<OrderDetailedRecord<Shipment>>{
	
	private ShipmentDTOForwarder dtoForwarder;
	
	private OrderDetailedRecord<Shipment> odr;

	public ShipmentODRResponseGenerator(OrderDetailedRecord<Shipment> odr){
		this.odr=odr;
		//dtoForwarder=new ShipmentDTOForwarder();
	}
	

	@Override
	public TotalWeightAndCostResponse produceResponse() throws DTObjectEmptyBodyException, NotValidObjectException{
		
		if(hasSomethingToProduceFrom()){
			Response response = getDtoForwarder().validateAndForward(this.odr);
			if(!(response instanceof NotValidObjectResponse))
				return (TotalWeightAndCostResponse)response;
			else throw new NotValidObjectException("Passed object is not a valid object in the current context!");
		}
		else throw new DTObjectEmptyBodyException("Data Transfer Object's body can not be empty!");

	}

	@Override
	public OrderDetailedRecord<Shipment> get() {
		return this.odr;
	}

	@Override
	public void set(OrderDetailedRecord<Shipment> object) {
		this.odr=object;	
	}

	@Override
	public boolean hasSomethingToProduceFrom() {
		return !odr.isEmpty();
	}


	public ShipmentDTOForwarder getDtoForwarder() {
		return dtoForwarder;
	}

	public void setDtoForwarder(ShipmentDTOForwarder dtoForwarder) {
		this.dtoForwarder = dtoForwarder;
	}
	
	
	

}
