package com.smsolutions.billing.integration.generation;

import java.util.List;

import com.smsolutions.billing.repository.impl.specific.entities.Order;


public class OrderHistoryResponse extends Response {
	
	private List<Order> orders;
	
    public OrderHistoryResponse(){
		
	}
	
    public OrderHistoryResponse(List<Order> orders){
		this.orders=orders;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

}
