package com.smsolutions.billing.integration.generation;

public interface IAccessible<T> {
	
	public T get();

}
