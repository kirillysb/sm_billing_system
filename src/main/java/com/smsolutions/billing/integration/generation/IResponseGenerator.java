package com.smsolutions.billing.integration.generation;

import com.smsolutions.billing.integration.exceptions.DTObjectEmptyBodyException;
import com.smsolutions.billing.integration.exceptions.NotValidObjectException;

public interface IResponseGenerator<T extends Response> {
	
	public T produceResponse() throws DTObjectEmptyBodyException, NotValidObjectException;
	public boolean hasSomethingToProduceFrom();

}
