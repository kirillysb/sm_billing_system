package com.smsolutions.billing.integration.generation;

public interface IReusable<T> extends IAccessible<T> {
	
	public void set(T object);

}
