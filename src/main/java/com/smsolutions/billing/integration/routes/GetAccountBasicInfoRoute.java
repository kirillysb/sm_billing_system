package com.smsolutions.billing.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;

import com.smsolutions.billing.integration.model.GetAccountBasicInfoRequest;
import com.smsolutions.billing.integration.model.GetAccountBasicInfoResponse;
import com.smsolutions.billing.repository.impl.specific.CustomerJpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.Customer;
import com.smsolutions.billing.repository.impl.specific.entities.Shipment;

public class GetAccountBasicInfoRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxb = new JaxbDataFormat(GetAccountBasicInfoRequest.class.getPackage().getName());
        
        from("spring-ws:rootqname:{billing/services}getAccountBasicInfoRequest?endpointMapping=#endpointMapping")
            .unmarshal(jaxb)
            .process(new UAccountProcessor())
            .marshal(jaxb);
    }
    
    private static final class UAccountProcessor implements Processor {
        public void process(Exchange exchange) throws Exception {
        	GetAccountBasicInfoRequest request = exchange.getIn().getBody(GetAccountBasicInfoRequest.class);
        	GetAccountBasicInfoResponse response = new GetAccountBasicInfoResponse();
            //double result = request.getTotalCost();
        	int id=request.getUID();
        	String pu="hibernateCustomUnit";
        	final CustomerJpaRepository rep=new CustomerJpaRepository(pu);
        	//rep.persist(new Customer("das","12"));
        	//int cust=rep.get().toArray().length;
        	if(rep.get(id).isPresent()) response.setExtractedCost(rep.get(id).get().getLastName());
        	else  response.setExtractedCost("Nothing");
            
            
            exchange.getOut().setBody(response);
        }
    }
   
}