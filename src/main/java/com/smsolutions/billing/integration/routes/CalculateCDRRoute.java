package com.smsolutions.billing.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.springframework.beans.factory.annotation.Autowired;

import com.smsolutions.billing.core.rating.RatingManager;
import com.smsolutions.billing.integration.generation.ShipmentODRResponseGenerator;
import com.smsolutions.billing.integration.model.CalculateCDRRequest;
import com.smsolutions.billing.integration.model.CalculateCDRResponse;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.Shipment;
import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;

public class CalculateCDRRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxb = new JaxbDataFormat(CalculateCDRRequest.class.getPackage().getName());
        
        from("spring-ws:rootqname:{billing/services}calculateCDRRequest?endpointMapping=#endpointMapping")
            .unmarshal(jaxb)
            .process(new CDRProcessor())
            .marshal(jaxb);
    }
    
    private static final class CDRProcessor implements Processor {
    	private ShipmentODRResponseGenerator responseGenerator;
        public void process(Exchange exchange) throws Exception {
        	CalculateCDRRequest request = exchange.getIn().getBody(CalculateCDRRequest.class);
        	CalculateCDRResponse response = new CalculateCDRResponse();
        	OrderDetailedRecord<Shipment> odr=new OrderDetailedRecord<Shipment>();
        	odr.addCollection(request.getShipments());
        	odr.setCurrency(request.getCurrency());
        	responseGenerator = new ShipmentODRResponseGenerator(odr);
        	TotalWeightAndCostResponse twacResponse=responseGenerator.produceResponse();
            //double result = request.getTotalCost();
        	//rm.loadElements(request.getShipments());
        	//rm.start();
        	//Shipment shipment = request.getShipments().get(0);
            //response.setExtractedCost(shipment.itemWeight);
        	response.setTotalCost(twacResponse.getTotalCost());
        	response.setTotalWeight(twacResponse.getTotalWeight());
            exchange.getOut().setBody(response);
        }
    }
   
}