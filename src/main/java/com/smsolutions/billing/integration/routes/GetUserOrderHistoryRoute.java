package com.smsolutions.billing.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;

import com.smsolutions.billing.integration.generation.OrderHistoryResponse;
import com.smsolutions.billing.integration.generation.OrderHistoryResponseGenerator;
import com.smsolutions.billing.integration.generation.ShipmentODRResponseGenerator;
import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;
import com.smsolutions.billing.integration.model.CalculateCDRRequest;
import com.smsolutions.billing.integration.model.CalculateCDRResponse;
import com.smsolutions.billing.integration.model.GetUserOrderHistoryRequest;
import com.smsolutions.billing.integration.model.GetUserOrderHistoryResponse;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.Shipment;

public class GetUserOrderHistoryRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxb = new JaxbDataFormat(GetUserOrderHistoryRequest.class.getPackage().getName());
        
        from("spring-ws:rootqname:{billing/services}getUserOrderHistoryRequest?endpointMapping=#endpointMapping")
            .unmarshal(jaxb)
            .process(new OrderHistoryProcessor())
            .marshal(jaxb);
    }
    
    private static final class OrderHistoryProcessor implements Processor {
    	private OrderHistoryResponseGenerator responseGenerator;
        public void process(Exchange exchange) throws Exception {
        	GetUserOrderHistoryRequest request = exchange.getIn().getBody(GetUserOrderHistoryRequest.class);
        	GetUserOrderHistoryResponse response = new GetUserOrderHistoryResponse();
        	responseGenerator = new OrderHistoryResponseGenerator(request.getUsernamae(),request.getIndexStart(),
        			request.getIndexStop());
        	OrderHistoryResponse twacResponse=responseGenerator.produceResponse();
            //double result = request.getTotalCost();
        	//rm.loadElements(request.getShipments());
        	//rm.start();
        	//Shipment shipment = request.getShipments().get(0);
            //response.setExtractedCost(shipment.itemWeight);
        	response.setOrders(twacResponse.getOrders());
            exchange.getOut().setBody(response);
        }
    }
   
}
