package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.WeightToCountryCost;

public class WeightToCountryCostJpaRepository extends JpaRepository<WeightToCountryCost> {

    public WeightToCountryCostJpaRepository(String persistenceUnitName) {
        super(WeightToCountryCost.class, persistenceUnitName);
    }

	public double getRangeIdByWCRAndCurrency(int wcr, int currencyId) {
		return runInTransaction(entityManager -> {
    		TypedQuery<Double> query = entityManager.
    				createQuery("SELECT c.cost FROM WeightToCountryCost AS c WHERE c.wcr = :wcr and c.currency = :currencyId",Double.class);
    		query.setParameter("wcr", wcr);
    		query.setParameter("currencyId", currencyId);
            return query.getSingleResult();
        });
	}
}

