package com.smsolutions.billing.repository.impl.specific.entities;

import java.util.Date;

public class Shipment {
	
	public Date shipmentDate;
	public String countryOfOrigin;
	public String countryOfDestination;
	public double itemWeight;
	public boolean isExpress;
	public double totalCost;
	public String currency;

}
