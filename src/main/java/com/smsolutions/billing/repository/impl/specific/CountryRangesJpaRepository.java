package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.CountryRange;

public class CountryRangesJpaRepository extends JpaRepository<CountryRange> {

    public CountryRangesJpaRepository(String persistenceUnitName) {
        super(CountryRange.class, persistenceUnitName);
    }
    
    public int findRangeByIds(int countryOfOriginId,int countryOfDestinationId){
    	return runInTransaction(entityManager -> {
    		TypedQuery<Integer> query = entityManager.
    				createQuery("SELECT c.id FROM CountryRange AS c WHERE (c.fromCn = :from_cn and c.toCn = :to_cn) "
    						+ "or (c.fromCn = :to_cn and c.toCn = :from_cn)",Integer.class);
    		query.setParameter("from_cn", countryOfOriginId);
    		query.setParameter("to_cn", countryOfDestinationId);
            return query.getSingleResult();
        });
    }
}
