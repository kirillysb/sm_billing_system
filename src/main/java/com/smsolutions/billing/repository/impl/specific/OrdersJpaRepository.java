package com.smsolutions.billing.repository.impl.specific;

import java.util.List;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.integration.generation.OrderHistoryResponse;
import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.Order;

public class OrdersJpaRepository extends JpaRepository<Order> {

    public OrdersJpaRepository(String persistenceUnitName) {
        super(Order.class, persistenceUnitName);
    }
    
    public List<Order> getOrderListByUsername(String username,int indexStart,int count){
    	return runInTransaction(entityManager -> {
    		TypedQuery<Order> query = entityManager.
    				createQuery("SELECT c FROM Order AS c WHERE c.username = :username",Order.class);
    		query.setParameter("username", username);
            query.setFirstResult(indexStart);
            query.setMaxResults(count);
            return query.getResultList();
        });
    }
    
    public OrderHistoryResponse getResponseByUsername(String username,int indexStart,int count){
    	return runInTransaction(entityManager -> {
    		TypedQuery<Order> query = entityManager.
    				createQuery("SELECT c FROM Order AS c WHERE c.username = :username",Order.class);
    		query.setParameter("username", username);
            query.setFirstResult(indexStart);
            query.setMaxResults(count);
            return new OrderHistoryResponse(query.getResultList());
        });
    }
    
}
