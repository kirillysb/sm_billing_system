package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.WeightRange;

public class WeightRangesJpaRepository extends JpaRepository<WeightRange> {

    public WeightRangesJpaRepository(String persistenceUnitName) {
        super(WeightRange.class, persistenceUnitName);
    }

	public int getRangeIdByWeight(double weight) {
		return runInTransaction(entityManager -> {
    		TypedQuery<Integer> query = entityManager.
    				createQuery("SELECT c.id FROM WeightRange AS c WHERE c.fromWeight <= :weight and c.toWeight >= :weight",Integer.class);
    		query.setParameter("weight", weight);
            return query.getSingleResult();
        });
	}
}
