package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "weight_to_country_cost")
public class WeightToCountryCost extends Identity {

    private int wcr;
    private int currency;
    private double cost;

    public WeightToCountryCost() {
    }
    
    public WeightToCountryCost(int wcr,int currency,double cost) {
        this.setWcr(wcr);
        this.setCurrency(currency);
        this.setCost(cost);
    }

    @Column(name = "wcr", nullable = false)
	public int getWcr() {
		return wcr;
	}

	public void setWcr(int wcr) {
		this.wcr = wcr;
	}

	@Column(name = "currency", nullable = false)
	public int getCurrency() {
		return currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	@Column(name = "cost", nullable = false)
	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

    
}