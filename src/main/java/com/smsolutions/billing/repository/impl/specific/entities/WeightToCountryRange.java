package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "weight_to_country_ranges")
public class WeightToCountryRange extends Identity {

    private int weight;
    private int country;

    public WeightToCountryRange() {
    }
    
    public WeightToCountryRange(int weight,int country) {
        this.weight=weight;
        this.country=country;
    }

    @Column(name = "weight", nullable = false)
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Column(name = "country", nullable = false)
	public int getCountry() {
		return country;
	}

	public void setCountry(int country) {
		this.country = country;
	}
    
}
