package com.smsolutions.billing.repository.impl.specific;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.Customer;

public class CustomerJpaRepository extends JpaRepository<Customer> {

    public CustomerJpaRepository(String persistenceUnitName) {
        super(Customer.class, persistenceUnitName);
    }
}
