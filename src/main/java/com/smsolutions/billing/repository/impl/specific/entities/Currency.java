package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "currencies")
public class Currency extends Identity {

    private String currencyQualifier; 

    public Currency() {
    }
    
    public Currency(String currencyQualifier) {
        this.currencyQualifier=currencyQualifier;
    }

    @Column(name = "currency_qualifier", nullable = false,length=30)
    public String getCurrencyQualifier() {
        return currencyQualifier;
    }

    public void setCurrencyQualifier(String currencyQualifier) {
        this.currencyQualifier = currencyQualifier;
    }
    
}
