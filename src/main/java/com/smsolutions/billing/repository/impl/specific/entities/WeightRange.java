package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "weight_ranges")
public class WeightRange extends Identity {

    private double fromWeight;
    private double toWeight;

    public WeightRange() {
    }
    
    public WeightRange(double fromWeight,double toWeight) {
        this.setFromWeight(fromWeight);
        this.setToWeight(toWeight);
    }


    @Column(name = "from_weight", nullable = false)
	public double getFromWeight() {
		return fromWeight;
	}

	public void setFromWeight(double fromWeight) {
		this.fromWeight = fromWeight;
	}

	@Column(name = "to_weight", nullable = false)
	public double getToWeight() {
		return toWeight;
	}

	public void setToWeight(double toWeight) {
		this.toWeight = toWeight;
	}
    
}
