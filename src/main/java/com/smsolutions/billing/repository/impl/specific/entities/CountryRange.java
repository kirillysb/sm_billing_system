package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "country_ranges")
public class CountryRange extends Identity {

	
    private int fromCn;
    private int toCn;
    //private Country country;
    //private Country countryOfDestination;

    public CountryRange() {
    }
    
   
    public CountryRange(int from_cn,int to_cn) {
        this.fromCn = from_cn;
        this.toCn=to_cn;
    }

    @Column(name = "from_cn", nullable = false)
    public int getFromCn() {
        return fromCn;
    }

    public void setFromCn(int from_cn) {
        this.fromCn = from_cn;
    }
    
    @Column(name = "to_cn", nullable = false)
    public int getToCn() {
        return toCn;
    }

    public void setToCn(int to_cn) {
        this.toCn = to_cn;
    }
    
   /* @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
    	 @JoinColumn(name="from_cn", referencedColumnName="id"),
    	 @JoinColumn(name="to_cn", referencedColumnName="id")
    })*/
    /*public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }*/
    
}
