package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.WeightToCountryRange;

public class WeightToCountryRangesJpaRepository extends JpaRepository<WeightToCountryRange> {

    public WeightToCountryRangesJpaRepository(String persistenceUnitName) {
        super(WeightToCountryRange.class, persistenceUnitName);
    }

	public int getRangeIdByWeightAndCountry(int weight, int country) {
		return runInTransaction(entityManager -> {
    		TypedQuery<Integer> query = entityManager.
    				createQuery("SELECT c.id FROM WeightToCountryRange AS c WHERE c.weight = :weight and c.country = :country",Integer.class);
    		query.setParameter("weight", weight);
    		query.setParameter("country", country);
            return query.getSingleResult();
        });
	}
}
