package com.smsolutions.billing.repository.impl.specific.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "orders")
public class Order extends Identity {
	
	private long orderIdentifier;
	private Date dateCreated;
	private double totalWeight;
	private double totalCost;
	private String username;
	
	public Order(){
		
	}
	
    public Order(long orderIdentifier,Date dateCreated,double totalWeight,
    		double totalCost,String username){
		this.orderIdentifier=orderIdentifier;
		this.dateCreated=dateCreated;
		this.totalWeight=totalWeight;
		this.totalCost=totalCost;
		this.username=username;
	}
	
    @Column(name = "orderIdentifier", nullable = false)
	public long getOrderIdentifier() {
		return orderIdentifier;
	}
	public void setOrderIdentifier(long orderIdentifier) {
		this.orderIdentifier = orderIdentifier;
	}
	
	@Column(name = "dateCreated", nullable = false)
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	@Column(name = "totalWeight", nullable = false)
	public double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	@Column(name = "totalCost", nullable = false)
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	@Column(name = "username", nullable = false,length=30)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
