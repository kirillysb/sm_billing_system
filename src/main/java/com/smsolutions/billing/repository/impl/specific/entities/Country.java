package com.smsolutions.billing.repository.impl.specific.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "countries")
public class Country extends Identity {

    private String name; 

    public Country() {
    }
    
    /*@Override
    @OneToOne(mappedBy="Country")
    public int getId() {
        return super.getId();
    }*/
    
    public Country(String name) {
        this.name=name;
    }

    @Column(name = "name", nullable = false,length=30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}