package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.Country;

public class CountryJpaRepository extends JpaRepository<Country> {

    public CountryJpaRepository(String persistenceUnitName) {
        super(Country.class, persistenceUnitName);
    }
    
    public int findByName(String name){
    	return runInTransaction(entityManager -> {
    		TypedQuery<Integer> query = entityManager.
    				createQuery("SELECT c.id FROM Country AS c WHERE c.name = :name",Integer.class);
    		query.setParameter("name", name);
            return query.getSingleResult();
        });
    }
}
