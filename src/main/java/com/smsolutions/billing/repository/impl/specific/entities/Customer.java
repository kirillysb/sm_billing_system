package com.smsolutions.billing.repository.impl.specific.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.smsolutions.billing.repository.impl.generic.Identity;

@Entity
@Table(name = "customers")
public class Customer extends Identity {

    private String firstName;
    private String lastName;

    Customer() {
    }
    
    public Customer(String firstName,String lastName) {
        this.firstName = firstName;
        this.lastName=lastName;
    }

    @Column(name = "firstName", nullable = false, length = 50)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    @Column(name = "lastName", nullable = false, length = 50)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
