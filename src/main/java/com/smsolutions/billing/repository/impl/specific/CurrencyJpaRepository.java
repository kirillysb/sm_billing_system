package com.smsolutions.billing.repository.impl.specific;

import javax.persistence.TypedQuery;

import com.smsolutions.billing.repository.impl.generic.JpaRepository;
import com.smsolutions.billing.repository.impl.specific.entities.Currency;

public class CurrencyJpaRepository extends JpaRepository<Currency> {

    public CurrencyJpaRepository(String persistenceUnitName) {
        super(Currency.class, persistenceUnitName);
    }

	public int findByName(String currencyName) {
		return runInTransaction(entityManager -> {
    		TypedQuery<Integer> query = entityManager.
    				createQuery("SELECT c.id FROM Currency AS c WHERE c.currencyQualifier = :currencyName",Integer.class);
    		query.setParameter("currencyName", currencyName);
            return query.getSingleResult();
        });
	}
}

