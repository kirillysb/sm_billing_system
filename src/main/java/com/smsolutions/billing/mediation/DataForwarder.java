package com.smsolutions.billing.mediation;

public class DataForwarder {
	
	private Validator validator;
	
	DataForwarder(){
		validator=new Validator();
	}
	
	protected boolean isObjectValid(Object obj,Class<?> interf){
		return validator.validateObjectByClass(obj, interf);
	}
	

}
