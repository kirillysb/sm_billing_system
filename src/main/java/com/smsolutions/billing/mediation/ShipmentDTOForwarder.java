package com.smsolutions.billing.mediation;

import com.smsolutions.billing.core.rating.ShipmentDTOProcessor;
import com.smsolutions.billing.integration.generation.NotValidObjectResponse;
import com.smsolutions.billing.integration.generation.Response;
import com.smsolutions.billing.mediation.dto.IDataTransferObject;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.Shipment;

public class ShipmentDTOForwarder extends DataTransferObjectForwarder<OrderDetailedRecord<Shipment>>{
	
	private ShipmentDTOProcessor processor;
	
	public ShipmentDTOForwarder(){
		super();
		processor=new ShipmentDTOProcessor();
	}
	
	@Override
	public Response validateAndForward(OrderDetailedRecord<Shipment> obj){
		if(isObjectValid(obj,IDataTransferObject.class)){
			 return processor.proc(obj);
		}
		return new NotValidObjectResponse();
	}

}
