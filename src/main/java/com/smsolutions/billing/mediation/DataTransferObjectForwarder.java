package com.smsolutions.billing.mediation;

import com.smsolutions.billing.integration.generation.TotalWeightAndCostResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.smsolutions.billing.core.rating.RatingManager;
import com.smsolutions.billing.core.rating.ShipmentDTOProcessor;
import com.smsolutions.billing.integration.generation.NotValidObjectResponse;
import com.smsolutions.billing.integration.generation.Response;
import com.smsolutions.billing.mediation.dto.IDataTransferObject;
import com.smsolutions.billing.mediation.dto.OrderDetailedRecord;
import com.smsolutions.billing.mediation.dto.OrderUnitObject;
import com.smsolutions.billing.mediation.dto.Shipment;

public class DataTransferObjectForwarder<T extends IDataTransferObject> extends DataForwarder {
	
	//@Autowired
	//
	
	public DataTransferObjectForwarder(){
		super();
	}

	public Response validateAndForward(T obj){
		return new NotValidObjectResponse();
	}
}
