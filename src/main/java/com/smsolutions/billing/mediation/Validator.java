package com.smsolutions.billing.mediation;

public class Validator implements IValidation{
	
	Validator(){
		
	}
	
	public boolean validateObjectByClass(Object obj,Class<?> interf){
		return interf.isInstance(obj);
	}

}
