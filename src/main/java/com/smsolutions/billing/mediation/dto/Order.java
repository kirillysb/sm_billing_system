package com.smsolutions.billing.mediation.dto;

import java.util.Date;

public class Order extends DataTransferUnit{
	
	private long orderIdentifier;
	private Date dateCreated;
	private double totalWeight;
	private double totalCost;
	
	public Order(){
		
	}
	
	public long getOrderIdentifier() {
		return orderIdentifier;
	}
	public void setOrderIdentifier(long orderIdentifier) {
		this.orderIdentifier = orderIdentifier;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
}
