package com.smsolutions.billing.mediation.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


public class OrderDetailedRecord<T extends OrderUnitObject> implements IDataTransferObject<T>,
                                                                                 Iterable<T>{
	
	private List<T> units;
	private String currency;
	
	public OrderDetailedRecord(List<T> units){
		this.units=units;
	}
	
	public OrderDetailedRecord(){
		units=new ArrayList<T>();
	}

	@Override
	public boolean isEmpty() {
		
		return units.isEmpty();
	}

	public void addCollection(Collection<T> collection){
		units.addAll(collection);
	}
	
	public void clear(){
		units.clear();
	}

	@Override
	public Iterator<T> iterator() {
		return units.iterator();
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
