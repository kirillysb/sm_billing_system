package com.smsolutions.billing.mediation.dto;

import java.util.Date;

public class Shipment extends OrderUnitObject{
	
	private Date shipmentDate;
	private String countryOfOrigin;
	private String countryOfDestination;
	private double itemWeight;
	private boolean isExpress;
	
	public Shipment(){
		
	}
	
	public Date getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getCountryOfDestination() {
		return countryOfDestination;
	}

	public void setCountryOfDestination(String countryOfDestination) {
		this.countryOfDestination = countryOfDestination;
	}

	/**
	 * @return the itemWeight
	 */
	public double getItemWeight() {
		return itemWeight;
	}

	/**
	 * @param itemWeight the itemWeight to set
	 */
	public void setItemWeight(double itemWeight) {
		this.itemWeight = itemWeight;
	}

	/**
	 * @return the isExpress
	 */
	public boolean getIsExpress() {
		return isExpress;
	}

	/**
	 * @param isExpress the isExpress to set
	 */
	public void setIsExpress(boolean isExpress) {
		this.isExpress = isExpress;
	}

}
