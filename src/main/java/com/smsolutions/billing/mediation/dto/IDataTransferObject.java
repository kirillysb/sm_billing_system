package com.smsolutions.billing.mediation.dto;

public interface IDataTransferObject<T extends DataTransferUnit> {
	
	public boolean isEmpty();

}
