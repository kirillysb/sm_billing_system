Fuse on EAP Apache Camel Billing Web Service Application
=============

This is a SOAP based retail billing web service application. 


Prerequisites
=============

* Minimum of Java 1.8
* Maven 3.2 or greater
* JBoss EAP 6.4
* Jboss Fuse 6.2.1
